from django.urls import path

from . import views

urlpatterns = [
    path("", views.dashboard),
    path("count-user", views.count_user),
    path("count-groups", views.count_groups),
    path("count-computers", views.count_computers),
    path("count-domains", views.count_domains),
    path("count-websites", views.count_websites),
    path("count-virtualmachines", views.count_virtualmachines),
    path("count-ucstack-usermailboxes", views.count_ucstack_usermailboxes),
    path("count-ucstack-resourcemailboxes", views.count_ucstack_resourcemailboxes),
    path("count-certificate-authorities", views.count_certificate_authorities),
    path("count-certificates", views.count_certificates),
    path("count-wifistack-nodes", views.count_wifistack_nodes),
    path("count-wifistack-domains", views.count_wifistack_domains),
    path("count-customers", views.count_customers),
]
