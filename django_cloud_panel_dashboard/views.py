from datetime import datetime

from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, JsonResponse
from django.template import loader
from django.utils.translation import gettext as _

from django_cloud_panel_main.django_cloud_panel_main.views import get_nav
from django_mdat_customer.django_mdat_customer.models import MdatCustomers
from django_directory_backend_client.django_directory_backend_client.func import *
from django_pki_manager_client.django_pki_manager_client.func import (
    pki_manager_get_certificate_authorities,
    pki_manager_get_certificates,
)
from django_session_ldap_attributes.django_session_ldap_attributes.decorators import (
    admin_group_required,
)
from django_session_ldap_attributes.django_session_ldap_attributes.snippets import (
    get_mdat_customer_for_user,
)
from django_whstack_client.django_whstack_client.func import (
    whstack_get_registrydomains,
    whstack_get_websites,
)
from django_wifistack_client.django_wifistack_client.func import (
    wifistack_get_wireless_nodes,
    wifistack_get_domains,
)
from ldap_essential_helpers.ldap_essential_helpers.ad import convert_ad_timestamp


@login_required
def dashboard(request):
    template = loader.get_template("django_cloud_panel_dashboard/dashboard.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Dashboard")
    template_opts["content_title_sub"] = _("Overview")

    template_opts["menu_groups"] = get_nav(request)

    # TODO: Implement status API
    # is the customer affected?
    #    messages.warning(
    #        request,
    #        _(<Api Response Text>)
    #    )

    epoch = convert_ad_timestamp(
        request.user.userldapattributes.json_data["pwdLastSet"][0]
    )
    pw_reset = datetime.utcnow() - epoch
    template_opts["reset_day"] = epoch
    template_opts["reset_days"] = pw_reset.days

    # TODO: connect to satus api
    template_opts["reported_incidents"] = {}

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("DIRECTORY")
def count_user(request):
    mdat = get_mdat_customer_for_user(request.user)

    all_directory_user = directory_backend_get_users(mdat.id)

    return_obj = {
        "id": 1,
        "name": "User",
        "count": len(all_directory_user),
        "color": "#4284f5",
        "icon": "icon-user",
    }

    return JsonResponse(return_obj)


@admin_group_required("DIRECTORY")
def count_groups(request):
    mdat = get_mdat_customer_for_user(request.user)

    all_directory_groups = directory_backend_get_groups(mdat.id)

    return_obj = {
        "id": 2,
        "name": "Groups",
        "count": len(all_directory_groups),
        "color": "#4284f5",
        "icon": "icon-users4",
    }

    return JsonResponse(return_obj)


@admin_group_required("DIRECTORY")
def count_computers(request):
    mdat = get_mdat_customer_for_user(request.user)

    all_directory_computer = directory_backend_get_computers(mdat.id)

    return_obj = {
        "id": 3,
        "name": "Computer",
        "count": len(all_directory_computer),
        "color": "#4284f5",
        "icon": "icon-laptop",
    }

    return JsonResponse(return_obj)


@admin_group_required("DOMAINS")
def count_domains(request):
    mdat = get_mdat_customer_for_user(request.user)

    all_whstack_domains = whstack_get_registrydomains(mdat.id)

    return_obj = {
        "id": 4,
        "name": "Domains",
        "count": len(all_whstack_domains),
        "color": "#37e677",
        "icon": "icon-sphere",
    }

    return JsonResponse(return_obj)


@admin_group_required("WWW")
def count_websites(request):
    mdat = get_mdat_customer_for_user(request.user)

    all_whstack_websites = whstack_get_websites(mdat.id)

    return_obj = {
        "id": 5,
        "name": "Websites",
        "count": len(all_whstack_websites),
        "color": "#37e377",
        "icon": "icon-list3",
    }

    return JsonResponse(return_obj)


@admin_group_required("COMPUTECLOUD")
def count_virtualmachines(request):
    mdat = get_mdat_customer_for_user(request.user)

    # TODO add all virtualmachines
    all_cloudstack_virtualmachines = ()

    return_obj = {
        "id": 6,
        "name": "Virtual Machines",
        "count": len(all_cloudstack_virtualmachines),
        "color": "#5a37e6",
        "icon": "icon-server",
    }

    return JsonResponse(return_obj)


@admin_group_required("UC")
def count_ucstack_usermailboxes(request):
    mdat = get_mdat_customer_for_user(request.user)

    # TODO add all ucstack_usermailboxes
    all_ucstack_usermailboxes = ()

    return_obj = {
        "id": 7,
        "name": "User Mailboxes",
        "count": len(all_ucstack_usermailboxes),
        "color": "#04e0d5",
        "icon": "icon-envelop",
    }

    return JsonResponse(return_obj)


@admin_group_required("UC")
def count_ucstack_resourcemailboxes(request):
    mdat = get_mdat_customer_for_user(request.user)

    # TODO add all ucstack_resourcemailboxes
    all_ucstack_resourcemailboxes = ()

    return_obj = {
        "id": 8,
        "name": "Resource Mailboxes",
        "count": len(all_ucstack_resourcemailboxes),
        "color": "#04e0d5",
        "icon": "icon-envelop2",
    }

    return JsonResponse(return_obj)


@admin_group_required("PKI")
def count_certificate_authorities(request):
    mdat = get_mdat_customer_for_user(request.user)

    all_pki_manager_certificate_authorities = pki_manager_get_certificate_authorities(
        mdat.id
    )

    return_obj = {
        "id": 9,
        "name": "Certificate Authorities",
        "count": len(all_pki_manager_certificate_authorities),
        "color": "#e6374b",
        "icon": "icon-certificate",
    }

    return JsonResponse(return_obj)


@admin_group_required("PKI")
def count_certificates(request):
    mdat = get_mdat_customer_for_user(request.user)

    all_pki_manager_certificates = pki_manager_get_certificates(mdat.id)

    return_obj = {
        "id": 10,
        "name": "Certificates",
        "count": len(all_pki_manager_certificates),
        "color": "#e6374b",
        "icon": "icon-medal",
    }

    return JsonResponse(return_obj)


@admin_group_required("WLAN")
def count_wifistack_nodes(request):
    mdat = get_mdat_customer_for_user(request.user)

    all_wifistack_nodes = wifistack_get_wireless_nodes(mdat.id)

    return_obj = {
        "id": 11,
        "name": "WiFi-Nodes",
        "count": len(all_wifistack_nodes),
        "color": "#dddd00",
        "icon": "icon-connection",
    }

    return JsonResponse(return_obj)


@admin_group_required("WLAN")
def count_wifistack_domains(request):
    mdat = get_mdat_customer_for_user(request.user)

    all_wifistack_domains = wifistack_get_domains(mdat.id)

    return_obj = {
        "id": 11,
        "name": "WiFi-Domains",
        "count": len(all_wifistack_domains),
        "color": "#dddd00",
        "icon": "icon-sphere",
    }

    return JsonResponse(return_obj)


@admin_group_required("VAR")
def count_customers(request):
    mdat = get_mdat_customer_for_user(request.user)

    mdat_customers = MdatCustomers(mdat.id).get_reseller_customers()
    all_customers = list(mdat_customers.values())

    return_obj = {
        "id": 12,
        "name": "Customers",
        "count": len(all_customers),
        "color": "#900C3F",
        "icon": "icon-user-tie",
    }

    return JsonResponse(return_obj)
